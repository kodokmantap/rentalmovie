//
//  MovieListTableViewController.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 20/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import UIKit

class MovieListTableViewController: UITableViewController {
    
    var movieList: [Movie]!
    var selectedMovie: Movie?
    var rentalPrice: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        movieList = MovieManager().movies

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return movieList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieList", for: indexPath) as! MovieListTableViewCell

        cell.movieTitleLabel.text = movieList[indexPath.row].name
        cell.movieGenreLabel.text = movieList[indexPath.row].genre.rawValue
        cell.movieRentalTypeLabel.text = movieList[indexPath.row].rentalType.rawValue
        cell.movieRentalPriceLabel.text = movieList[indexPath.row].rentalType.rawValue
        cell.moviePoster.image = UIImage(named: movieList[indexPath.row].posterId)
        // Configure the cell...
        
        switch movieList[indexPath.row].rentalType {
        case .hot:
            let price = movieList[indexPath.row].rentalPrice
            cell.movieRentalPriceLabel.text = "Rp \(price),00"
        case .discounted:
            let price = Int(Double(movieList[indexPath.row].rentalPrice) * 0.5)
            cell.movieRentalPriceLabel.text = "Rp \(price),00"
        default:
            let price = movieList[indexPath.row].rentalPrice
            cell.movieRentalPriceLabel.text = "Rp \(price),00"
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedMovie = movieList[indexPath.row]
        
        switch selectedMovie?.rentalType {
        case .hot:
            rentalPrice = selectedMovie?.rentalPrice
        case .discounted:
            rentalPrice = Int(Double(selectedMovie!.rentalPrice) * 0.5)
        default:
            rentalPrice = selectedMovie?.rentalPrice
        }
        performSegue(withIdentifier: "movieSelected", sender: self)
    }
}
