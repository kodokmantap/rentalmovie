//
//  ViewController.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 19/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var addMovieButtonOutlet: UIButton!
    @IBOutlet weak var movieTitleOutlet: UILabel!
    @IBOutlet weak var cityButtonOutlet: UIButton!
    @IBOutlet weak var postalCodeButtonOutlet: UIButton!
    @IBOutlet weak var rentButtonOutlet: UIButton!
        @IBOutlet weak var rentalPrice: UILabel!
    
    var selectedCity: City?
    var selectedPostalCode: String?
    var selectedMovie: Movie?
    var renterName: String?
    var totalRentingPrice: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        cityButtonOutlet.setTitle("Select city", for: .normal)
        postalCodeButtonOutlet.setTitle("Select postal code", for: .normal)
        movieTitleOutlet.text = selectedMovie?.name ?? "Add movie"
        rentButtonOutlet.setTitle("Choose movie first", for: .normal)
        rentButtonOutlet.backgroundColor = .lightGray
    }

    @IBAction func renterNameTextField(_ sender: UITextField) {
        renterName = sender.text
    }
    
    @IBAction func rentButton(_ sender: UIButton) {
        let alert = UIAlertController(title: "Are you ready \(renterName ?? "")?", message: "\(selectedMovie?.name ?? "movie") will be sent to your house \(selectedCity?.name ?? "") soon!", preferredStyle: .alert)
        alert.addAction(.init(title: "OK", style: .default, handler: { _ in
            print("OK")
        }))
        alert.addAction(.init(title: "Oh wait..", style: .destructive, handler: { _ in
            print("Cancel")
        }))
        
        self.present(alert, animated: true, completion: {
        })
    }
    
    // MARK: - UNWIND SEGUEs
    @IBAction func movieSelectedUnwind(segue:UIStoryboardSegue) {
        
        let source = segue.source as! MovieListTableViewController
        selectedMovie = source.selectedMovie
        movieTitleOutlet.text = selectedMovie?.name ?? "Add movie"
        addMovieButtonOutlet.setBackgroundImage(UIImage(named: selectedMovie?.posterId ?? "no-selected"), for: .normal)
        addMovieButtonOutlet.setTitle("", for: .normal)
        rentButtonOutlet.isEnabled = true
        rentButtonOutlet.titleLabel?.text = "Rent"
        rentButtonOutlet.backgroundColor = .systemBlue
        totalRentingPrice = source.rentalPrice
        rentalPrice.text = "Rp \(totalRentingPrice!),00"
    }
    
    @IBAction func selectedCityUnwind(segue:UIStoryboardSegue) {
        let source = segue.source as! CityTableViewController
        selectedCity = source.selectedCity
        cityButtonOutlet.setTitle(selectedCity?.name, for: .normal)
        postalCodeButtonOutlet.isEnabled = true
    }
    
    @IBAction func postalCodeSelectedUnwind(segue:UIStoryboardSegue) {
        let source = segue.source as! PostalCodeTableViewController
        selectedPostalCode = source.selectedPostalCode
        postalCodeButtonOutlet.setTitle(selectedPostalCode, for: .normal)
    }
    
    // MARK: - PREPARE
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSelectPostalCode" {
            
            if let destination = segue.destination as? PostalCodeTableViewController {
                destination.whichCity = selectedCity
                destination.title = selectedCity?.name
            }
            
        }
    }
}

