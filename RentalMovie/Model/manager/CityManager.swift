//
//  CityManager.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 20/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import Foundation

class CityManager {
    var cities: [City]
    
    init() {
        cities = [
            City(id: 1, name: "Jakarta", PostalCode: ["111","222","333","444"]),
            City(id: 2, name: "Surabaya", PostalCode: ["555","666","777","888"]),
            City(id: 3, name: "Bandung", PostalCode: ["999","100","101"])
        ]
    }
}
