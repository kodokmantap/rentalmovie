//
//  MovieManager.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 20/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import Foundation

class MovieManager {
    
    var movies: [Movie]
    
    init() {
        movies = [
            Movie(id: 1, name: "1917", genre: .action, rentalType: .hot, rentalPrice: 10_000, posterId: "1917"),
            Movie(id: 2, name: "Joker", genre: .drama, rentalType: .hot, rentalPrice: 12_000, posterId: "joker"),
            Movie(id: 3, name: "Avengers: The End Game", genre: .action, rentalType: .discounted, rentalPrice: 8_000, posterId: "endgame"),
            Movie(id: 4, name: "Evangelion 2.22", genre: .drama, rentalType: .discounted, rentalPrice: 8_000, posterId: "eva"),
            Movie(id: 5, name: "Star Trek", genre: .scifi, rentalType: .normal, rentalPrice: 10_000, posterId: "startrek"),
            Movie(id: 6, name: "Doraemon", genre: .scifi, rentalType: .discounted, rentalPrice: 6_000, posterId: "doraemon")
        ]
    }
}
