//
//  RentalType.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 20/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import Foundation

enum RentalType: String {
    case hot = "Hot"
    case normal = " "
    case discounted = "Discounted"
}
