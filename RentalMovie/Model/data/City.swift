//
//  City.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 20/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import Foundation

struct City {
    let id: Int
    let name: String
    let PostalCode: [String]
}
