//
//  Movie.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 19/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import Foundation

public struct Movie {
    let id: Int
    let name: String
    let genre: Genre
    let rentalType: RentalType
    let rentalPrice: Int
    let posterId: String
}
