//
//  Genre.swift
//  RentalMovie
//
//  Created by Yulibar Husni on 19/05/20.
//  Copyright © 2020 Yulibar Husni. All rights reserved.
//

import Foundation

enum Genre: String {
    case action = "Action"
    case drama = "Drama"
    case scifi = "Sci-Fi"
}
